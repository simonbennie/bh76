# imports
import glob
import sys
import os
from os.path import basename
from pprint import pprint
import numpy
import csv


#Convert to minesota style

# Read in folder name in current directory as command line argument.
try:
    dir_name = sys.argv[1]
    # print(dir_name)
except:
    print('Please pass directory name.')
    exit()

# Loop through chemical species (names of xyz files in directory)
for filename in glob.iglob("%s/**/*.xyz" % dir_name, recursive=True):
    #print(filename)
    fileonly = os.path.relpath(filename, dir_name)
    fileonly = os.path.dirname(fileonly)
    print(fileonly)
    print(filename, dir_name+"/"+fileonly+".xyz")
    os.rename(filename, dir_name+"/"+fileonly+".xyz")


#print(counter)

