# imports
import glob
import sys
import os
from os.path import basename
from pprint import pprint
import numpy
import csv

# This script takes in a folder as a command line argument, which should contain some number of matching species.xyz and species.g09 files.
# A new folder 'inputfoldername_MOLPRO' is created with a directory structure, where appropriate Molpro input files are created.

# Library of Molpro functionals
mp_functional = ["B", "B-LYP", "B-P", "B-VWN", "B3LYP3", "B3LYP5", "B88X", "B97", "B97R", "BECKE", "BH-LYP",
                 "CS", "D", "HFB", "HFS",
                 "LDA", "LSDAC", "LSDC", "LYP88", "MM05", "MM05-2X", "MM06", "MM06-2X", "MM06-L", "MM06-HF", "PBE",
                 "PBE0", "PBE0MOL",
                 "PBEREV", "PW91", "S", "S-VWN", "SLATER", "VS99", "VWN", "VWN80",
                 "M05", "M05-2X", "M06", "M06-2X", "M06-L", "M06-HF", "M08-HX", "M08-SO", "M11-L", "SOGGA", "SOGGA11"] #!,
# Desnity corrected functionals
mp_dc_functional = ["B-LYP","B3LYP3", "B3LYP5", "BH-LYP", "LDA","PBE", "PBE0", "M06", "M06-2X", "M06-L", "M06-HF"]#!,
# Other non-DFT methods
molpro_correlation = ["df-lrmp2","rDCSD","uDCSD","df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX)"]
# df-luccsd-f12 works with the following basi sets : avtz, avdz,svp,tzvp,qzvp
# df-lrmp works with svp tzvp,qzvp,avdz,avtz
# reference methods
molpro_reference = ["uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit",]
# Library of basis sets to be used
basis_sets = ["sto-3g", "SV(P)", "svp", "tzvp", "qzvp", "avdz", "avtz"]

# Dictionary of atomic symbols with atomic numbers, to be used for electron counting.
atomicNumbers = {'h': 1, 'he': 2, 'li': 3, 'be': 4, 'b': 5, 'c': 6, 'n': 7, 'o': 8, 'f': 9, 'ne': 10, 'p': 15, 's': 16,
                 'cl': 17, 'si': 14, 'zn': 30, 'mg': 12, 'v': 32, 'cr': 24, 'ni': 28, 'ar': 18,}

# Different MOLPRO input file strings, to be modified by script.
mp_header = '''memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
'''
ibos = '''{ibba,bonds=1}
'''
mp_footer = '''}
basis={ default,%s }
{%s%s,direct;%swf,%d,1,%d}
'''
mp_footer_dc = '''}
basis={ default,%s }
{%shf,direct;wf,%d,1,%d}
{%s%s,direct;%smaxit,0;wf,%d,1,%d}
'''
# Footer content:
# { default, basis set }
# { (ks,/uks,), functional/method, direct;wf, nelectrons, symmetry(1), (multiplicity-1) }

mp_footer_CCSD = '''}
basis={ default,%s }
{hf;wf,%d,1,%d}
{%s%s,direct;wf,%d,1,%d}
'''


#################################################
def molpro_write(geom, method, modifier1, modifier2, bset,density):
    # Write out a Molpro file in its own folder, within a specified directory temp_dir.
    # 'modifier1' edits the footer string (used for ks/uks).
    # 'modifier2' edits the file name (used for including -u).

    # Create folder of appropriate name, changing * to X and deleting brackets
    if ("luCCSD"in method):
        method_name = "luCCSD"
    elif ("uCCSD"in method):
        method_name = "uCCSD"
    else:
        method_name = method
    if ("DC"in density):
        method_name = "dc"+method
    if ("D3"in density):
        method_name = "D3"+method
    # Clean the SV(p) basis set so file names dont crash
    set_clean = bset.replace('(', '-').replace(')', '_')
    input_name = str(base_name + "_" + method_name.replace(')', '').replace('(', '') + "_" + set_clean + modifier2)
    print(input_name)
    # input_name = str(base_name + "-" + method.replace(')','').replace('(','') + "-" + set.replace('*','X') + modifier2)

    #geom = geom.astype(str)
    # Create file inside this directory, with the Molpro input file format.
    with open(temp_dir + "/" + input_name + ".com", 'w') as output:
        output.write(mp_header)
        for line in geom:
            output.write(line)

        # with open(temp_dir + "/" + input_name + ".com", 'ab') as output:
        #numpy.savetxt(output, geom, delimiter=' ',
        #              fmt="%s")  # ,header=mp_header,footer=mp_footer) #" %6.5f %6.5f %6.5f")


        # Check if CCSD method and if so write a different footer (so that HF calculation is done first)
        # with open(temp_dir + "/" + input_name + ".com", 'a') as output:
        if "CCSD" in method:
            output.write(mp_footer_CCSD % (bset, nelectrons, spin, modifier1, method, nelectrons, spin))
        elif method in molpro_correlation:
            output.write(mp_footer_CCSD % (bset, nelectrons, spin, modifier1, method, nelectrons, spin))

        #Generate density corrected functionals
        elif "DC" in density:
            if("u" in modifier1):
                unrestrict="u"
            else:
                if ("df" in density):
                    unrestrict="DF-"
                else:
                    unrestrict=""
            output.write(mp_footer_dc % (bset,unrestrict, nelectrons, spin, modifier1, method,"", nelectrons, spin))
        #Dispertion correction
        elif "D3" in density:
            output.write(mp_footer % (bset, modifier1, method,"disp;", nelectrons, spin))
            #output.write(mp_footer % (bset,modifier1, nelectrons, spin, modifier1, method,"disp;", nelectrons, spin))
        else:
            output.write(mp_footer % (bset, modifier1, method,"", nelectrons, spin))
        output.write(ibos)


def numberelectrons(geom, charge):
    # Take in raw geometry text and work out the number of electrons, accounting for overall charge.
    #print([line.split()[0] for line in geom],"hhshshshs \n")
    print(line.split()[0] for line in geom)
    atoms = [line.split()[0] for line in geom]
    atoms = [item.lower() for item in atoms]
    #print(sum([atomicNumbers[element] for element in atoms]),charge)
    neutral_electrons = sum([atomicNumbers[element] for element in atoms])
    return (neutral_electrons - charge)


#################################################


# Read in folder name in current directory as command line argument.
try:
    dir_name = sys.argv[1]
    # print(dir_name)
except:
    print('Please pass directory name.')
    exit()

# Create new directory to store input files in with.
dir_name_molpro = dir_name + "_Molpro"
try:
    os.mkdir(dir_name_molpro)
except:
    print('New directory already exists.')
    # exit()

# Read in the xyz files and if necassary the spin states of the molecules.
xyzfile = glob.glob("./%s/*.xyz" % dir_name)  # All xyz files read in.
counter = 0
# Loop through chemical species (names of xyz files in directory)
for filename in xyzfile:
    # Read geometry file, and remove first two lines.
    with open(filename) as f:
        # geom=list(reader)
        lines=f.readlines()
        #geom = numpy.array(f.readlines())
    geom=numpy.array(lines)
    geom=lines[2:]
    #geom = numpy.delete(geom, (0), axis=0)
    #geom = numpy.delete(geom, (0), axis=0)

    pprint(geom)

    # Create directory for different molecules.
    base_name = os.path.splitext(os.path.basename(filename))[0]

    # Open the matching .g09 file for the species.
    #try:
    #    gfile = open(filename[:-4] + ".g09")
    #except:
    #    print "Error opening Gaussian file."
    #glines = gfile.readlines()
    #gfile.close()

    # Attempt to read charge and spin from the Gaussian file.
    # This starts at line 8 and keeps going for some number of iterations,
    # until we find line of form "integer1 integer2". This is important as the
    # appropriate line of the Gaussian file can vary (e.g. in NHTBH38 database).
    charge_spin = None
    charge = None
    spin = None
    #for attempt in range(0, 2):
    #    #print(attempt)
    #    try:
    #        #print(glines[8])
    #        #print(glines[8 + attempt].split())
    #        charge_spin = glines[8 + attempt].split()
    #        charge = int(charge_spin[0])
    #        spin = int(charge_spin[1]) - 1  # In Molpro 2S format (multiplicity - 1)
    #        #print("Charge_spin", charge_spin)
    #        #print("Charge", charge)
    #        #print("Spin", spin)
    #        break
    #    except:
    #         continue

    charge=0
    spin=0
    # Number of electrons in molecule.
    #try:
    nelectrons = numberelectrons(geom, charge)
    print("trying to set multiplicity")
    print(filename,numberelectrons(geom, charge))
    print("Charge", charge)
    print("Spin", spin)
    #except Exception as inst:
    #    print(type(inst))
    #    print("Problem with reading charge/multiplicity - possibly comma separated values in: " + base_name)
    #exit()

    # Loop through basis functions
    for bset in basis_sets:
        # temp_dir = dir_name_molpro + "/" + base_name + "/" + set.replace('*','X')
        temp_dir = dir_name_molpro  # 1+ "/" + base_name + "/" + set.replace('*','X')
        #print(temp_dir)
        print(bset)
        # Attempt to create basis set directory.
        # try:
        #    os.mkdir(temp_dir)
        # except:
        #    print("Error creating basis set directory.")
        #    pass
        #Loop through the small basis sets without polarisation
        # Loop through functionals (DFT).
        if ("sto-3g" in bset) or ("SV(P)" in bset):
            for functional in mp_functional:
                # Write out appropriate Molpro input files by calling function.
                molpro_write(geom, functional, "ks,", "", bset,"SCF")
                molpro_write(geom, functional, "uks,", "_u", bset,"SCF")
                counter = counter + 2


            #Desnity corrected methods
            for functional in mp_dc_functional:
                molpro_write(geom, functional, "ks,", "", bset,"DC")
                molpro_write(geom, functional, "uks,", "_u", bset,"DC")
                #dispertion correction
                molpro_write(geom, functional, "ks,", "", bset,"D3")
                molpro_write(geom, functional, "uks,", "_u", bset,"D3")
                counter = counter + 4

            #density fitted methods dont work with small basis sets
            for reference in molpro_correlation:
                if ("df" in reference):
                    print("skipping")
                else:
                    molpro_write(geom, reference, "", "", bset,"null")
                    counter = counter + 1  # Print total number of input files generated
        else :
            for functional in mp_functional:
                # Write out appropriate Molpro input files by calling function.
                # Density fit big basis
                molpro_write(geom, functional, "df-ks,", "", bset,"DF-SCF")
                molpro_write(geom, functional, "uks,", "_u", bset,"DF-SCF")
                counter = counter + 2


            # Desnity corrected functionals
            for functional in mp_dc_functional:
                molpro_write(geom, functional, "df-ks,", "", bset,"DF-DC")
                molpro_write(geom, functional, "uks,", "_u", bset,"DF-DC")
                # dispertion correction
                molpro_write(geom, functional, "df-ks,", "", bset,"D3")
                molpro_write(geom, functional, "uks,", "_u", bset,"D3")
                counter = counter + 4

            for reference in molpro_correlation:
                # Skip full DCSD in big basis sets
                if ("df" in reference):
                    print(reference)
                    molpro_write(geom, reference, "", "", bset,"null")
                    counter = counter + 1  # Print total number of input files generated
                else:
                    if ("svp" in bset):
                        molpro_write(geom, reference, "", "", bset,"null")
                        counter = counter + 1  # Print total number of input files generated

        #Generate refernce wavefunctions.
        if bset is "avtz":
            for reference in molpro_reference:
                print("creating CCSD calculation")
                molpro_write(geom, reference, "", "", bset,"null")
                counter = counter + 1  # Print total number of input files generated
print(counter)

