memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000      0.45769122
   H          0.00000000      0.00000000     -0.45769122
}
basis={ default,avtz }
{df-ks,M06-HF,direct;disp;wf,10,1,0}
{ibba,bonds=1}
