memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000      0.42424634
   H         -0.80231209      0.00000000     -0.21212317
   H          0.80231209      0.00000000     -0.21212317
}
basis={ default,avtz }
{df-ks,PBE0,direct;disp;wf,9,1,0}
{ibba,bonds=1}
