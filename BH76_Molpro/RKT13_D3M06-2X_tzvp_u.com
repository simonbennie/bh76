memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      1.48580271
   CL         0.00000000      0.00000000      0.00000000
   H          0.00000000      0.00000000     -1.48580271
}
basis={ default,tzvp }
{uks,M06-2X,direct;disp;wf,19,1,0}
{ibba,bonds=1}
