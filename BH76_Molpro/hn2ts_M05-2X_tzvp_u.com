memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.42281633     -0.96904807      0.00000000
   N          0.42281633      0.15376339      0.00000000
   H         -0.84563265      0.81528469      0.00000000
}
basis={ default,tzvp }
{uks,M05-2X,direct;wf,15,1,0}
{ibba,bonds=1}
