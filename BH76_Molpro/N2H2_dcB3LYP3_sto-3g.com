memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.98497349      0.91525439      0.00000000
   N          0.00000000      0.62159276      0.00000000
   N          0.00000000     -0.62159276      0.00000000
   H         -0.98497349     -0.91525439      0.00000000
}
basis={ default,sto-3g }
{hf,direct;wf,16,1,0}
{ks,B3LYP3,direct;maxit,0;wf,16,1,0}
{ibba,bonds=1}
