memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   S          0.00000000      0.00000000      0.67010165
   H          0.00000000      0.00000000     -0.67010165
}
basis={ default,tzvp }
{uks,VWN80,direct;wf,17,1,0}
{ibba,bonds=1}
