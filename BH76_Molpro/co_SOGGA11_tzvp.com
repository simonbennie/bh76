memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.56480437
   C          0.00000000      0.00000000     -0.56480437
}
basis={ default,tzvp }
{df-ks,SOGGA11,direct;wf,14,1,0}
{ibba,bonds=1}
