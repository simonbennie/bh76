memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   S          0.00000000      0.00000000      0.61511669
   H         -0.96625120      0.00000000     -0.30755835
   H          0.96625120      0.00000000     -0.30755835
}
basis={ default,avtz }
{uks,M06-2X,direct;wf,18,1,0}
{ibba,bonds=1}
