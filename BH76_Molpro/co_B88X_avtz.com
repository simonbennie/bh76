memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.56480437
   C          0.00000000      0.00000000     -0.56480437
}
basis={ default,avtz }
{df-ks,B88X,direct;wf,14,1,0}
{ibba,bonds=1}
