memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000     -0.81516580
   CL         0.00000000      0.00000000      0.81516580
}
basis={ default,svp }
{uks,M06-HF,direct;disp;wf,26,1,0}
{ibba,bonds=1}
