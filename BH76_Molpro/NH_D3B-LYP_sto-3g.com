memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000      0.51836669
   H          0.00000000      0.00000000     -0.51836669
}
basis={ default,sto-3g }
{ks,B-LYP,direct;disp;wf,8,1,0}
{ibba,bonds=1}
