memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C         -0.59511855      0.05553194      0.07183342
   H         -0.94614299     -0.87691224     -0.34996000
   H         -0.79484063      0.94824655     -0.50588526
   H         -0.66963729      0.15353610      1.14674270
   H          0.78547631     -0.07316127     -0.09468485
   CL         2.22026316     -0.20724108     -0.26804601
}
basis={ default,avtz }
{hf;wf,27,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,27,1,0}
{ibba,bonds=1}
