memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      0.00000000
   H          0.00000000      0.00000000      0.92947338
   H          0.00000000      0.00000000     -0.92947338
}
basis={ default,tzvp }
{uks,MM05,direct;wf,3,1,0}
{ibba,bonds=1}
