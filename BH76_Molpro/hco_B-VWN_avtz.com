memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H         -0.60861063      0.82103438      0.00000000
   C          0.30430532      0.17779826      0.00000000
   O          0.30430532     -0.99883264      0.00000000
}
basis={ default,avtz }
{df-ks,B-VWN,direct;wf,15,1,0}
{ibba,bonds=1}
