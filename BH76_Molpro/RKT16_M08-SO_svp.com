memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          1.26209859      0.61673002      0.00000000
   S          0.00000026      1.05998000      0.00000000
   H         -0.50057601     -0.27862042      0.00000000
   H         -0.76152284     -1.39808960      0.00000000
}
basis={ default,svp }
{df-ks,M08-SO,direct;wf,19,1,0}
{ibba,bonds=1}
