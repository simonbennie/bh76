memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.48444828
   H          0.00000000      0.00000000     -0.48444828
}
basis={ default,sto-3g }
{uks,HFS,direct;wf,9,1,0}
{ibba,bonds=1}
