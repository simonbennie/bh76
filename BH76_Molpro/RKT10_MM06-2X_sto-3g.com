memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.14656781     -0.24726460      0.00000000
   F          0.00000000      1.21154849      0.00000000
   H         -0.14656781     -0.96428389      0.00000000
}
basis={ default,sto-3g }
{ks,MM06-2X,direct;wf,11,1,0}
{ibba,bonds=1}
