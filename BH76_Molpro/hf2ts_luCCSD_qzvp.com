memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000     -1.57015709
   F          0.00000000      0.00000000      0.04490245
   F          0.00000000      0.00000000      1.52525464
}
basis={ default,qzvp }
{hf;wf,19,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,19,1,0}
{ibba,bonds=1}
