memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N         -0.31221246      0.94105672      0.00000000
   N         -0.31221246     -0.23714479      0.00000000
   H          0.62442492     -0.70391193      0.00000000
}
basis={ default,avtz }
{uhf,direct;wf,15,1,0}
{uks,M06,direct;maxit,0;wf,15,1,0}
{ibba,bonds=1}
