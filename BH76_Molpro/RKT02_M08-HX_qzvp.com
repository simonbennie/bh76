memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O         -0.82792559     -0.29713382      0.00000000
   H         -0.95480536      0.66248308      0.00000000
   H          0.48862454     -0.28945176      0.00000000
   H          1.29410640     -0.07589751      0.00000000
}
basis={ default,qzvp }
{df-ks,M08-HX,direct;wf,11,1,0}
{ibba,bonds=1}
