memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.98497349      0.91525439      0.00000000
   N          0.00000000      0.62159276      0.00000000
   N          0.00000000     -0.62159276      0.00000000
   H         -0.98497349     -0.91525439      0.00000000
}
basis={ default,qzvp }
{df-ks,B3LYP5,direct;disp;wf,16,1,0}
{ibba,bonds=1}
