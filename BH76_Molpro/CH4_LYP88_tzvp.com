memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C          0.00000000      0.00000000      0.00000000
   H          0.62783705     -0.62783705      0.62783705
   H         -0.62783705      0.62783705      0.62783705
   H         -0.62783705     -0.62783705     -0.62783705
   H          0.62783705      0.62783705     -0.62783705
}
basis={ default,tzvp }
{df-ks,LYP88,direct;wf,10,1,0}
{ibba,bonds=1}
