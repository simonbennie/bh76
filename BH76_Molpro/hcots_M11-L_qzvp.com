memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H         -1.08633280      0.93797902      0.00000000
   C          0.54316640      0.09847636      0.00000000
   O          0.54316640     -1.03645538      0.00000000
}
basis={ default,qzvp }
{df-ks,M11-L,direct;wf,15,1,0}
{ibba,bonds=1}
