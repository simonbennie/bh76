memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.00000000
}
basis={ default,tzvp }
{df-ks,M08-HX,direct;wf,8,1,0}
{ibba,bonds=1}
