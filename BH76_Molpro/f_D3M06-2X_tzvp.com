memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000      0.00000000
}
basis={ default,tzvp }
{df-ks,M06-2X,direct;disp;wf,9,1,0}
{ibba,bonds=1}
