memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000     -0.81516580
   CL         0.00000000      0.00000000      0.81516580
}
basis={ default,avdz }
{hf;wf,26,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,26,1,0}
{ibba,bonds=1}
