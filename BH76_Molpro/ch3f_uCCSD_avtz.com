memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C          0.00000000      0.00000000      0.06556637
   F          0.00000000      0.00000000     -1.31562859
   H          0.51491633     -0.89186125      0.41668741
   H          0.51491633      0.89186125      0.41668741
   H         -1.02983267      0.00000000      0.41668741
}
basis={ default,avtz }
{hf;wf,18,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,18,1,0}
{ibba,bonds=1}
