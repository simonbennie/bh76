memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000     -1.14272669
   N          0.00000000      0.00000000     -0.02216159
   O          0.00000000      0.00000000      1.16488828
}
basis={ default,qzvp }
{uks,M06-2X,direct;disp;wf,22,1,0}
{ibba,bonds=1}
