memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000      0.54855572
   N          0.00000000      0.00000000     -0.54855572
}
basis={ default,avtz }
{uhf,direct;wf,14,1,0}
{uks,LDA,direct;maxit,0;wf,14,1,0}
{ibba,bonds=1}
