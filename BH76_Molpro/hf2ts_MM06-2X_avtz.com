memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000     -1.57015709
   F          0.00000000      0.00000000      0.04490245
   F          0.00000000      0.00000000      1.52525464
}
basis={ default,avtz }
{df-ks,MM06-2X,direct;wf,19,1,0}
{ibba,bonds=1}
