memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C          0.00000000      0.00000000      0.00000000
   H          0.53865812      0.93298323      0.00000000
   H          0.53865812     -0.93298323      0.00000000
   H         -1.07731624      0.00000000      0.00000000
}
basis={ default,avdz }
{df-ks,HFS,direct;wf,9,1,0}
{ibba,bonds=1}
