memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.00000000      0.00000000      0.63722413
   H          0.00000000      0.00000000     -0.63722413
}
basis={ default,avdz }
{df-ks,VWN80,direct;wf,18,1,0}
{ibba,bonds=1}
