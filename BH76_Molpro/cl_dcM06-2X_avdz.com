memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.00000000      0.00000000      0.00000000
}
basis={ default,avdz }
{hf,direct;wf,17,1,0}
{df-ks,M06-2X,direct;maxit,0;wf,17,1,0}
{ibba,bonds=1}
