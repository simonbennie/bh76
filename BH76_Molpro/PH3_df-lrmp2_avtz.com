memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   P          0.00000000      0.00000000      0.56884914
   H          0.59567102      1.03173246     -0.18961638
   H          0.59567102     -1.03173246     -0.18961638
   H         -1.19134203      0.00000000     -0.18961638
}
basis={ default,avtz }
{hf;wf,18,1,0}
{df-lrmp2,direct;wf,18,1,0}
{ibba,bonds=1}
