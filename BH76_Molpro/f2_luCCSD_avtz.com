memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000      0.69760378
   F          0.00000000      0.00000000     -0.69760378
}
basis={ default,avtz }
{hf;wf,18,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,18,1,0}
{ibba,bonds=1}
