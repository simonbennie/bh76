memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000      0.69760378
   F          0.00000000      0.00000000     -0.69760378
}
basis={ default,sto-3g }
{uks,M06-L,direct;disp;wf,18,1,0}
{ibba,bonds=1}
