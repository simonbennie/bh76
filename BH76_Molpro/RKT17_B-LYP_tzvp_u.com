memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.16310278     -1.28988895      0.00000000
   H         -0.32620556      0.09689412      0.00000000
   O          0.16310278      1.19299483      0.00000000
}
basis={ default,tzvp }
{uks,B-LYP,direct;wf,26,1,0}
{ibba,bonds=1}
