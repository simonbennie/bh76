memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.48102212
   H          0.00000000      0.00000000     -0.48102212
}
basis={ default,qzvp }
{uks,M06-L,direct;wf,9,1,0}
{ibba,bonds=1}
