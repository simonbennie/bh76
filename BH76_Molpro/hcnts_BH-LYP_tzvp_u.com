memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C          0.37482171      0.51783010      0.00000000
   N          0.37482171     -0.67052208      0.00000000
   H         -0.74964343      0.15269198      0.00000000
}
basis={ default,tzvp }
{uks,BH-LYP,direct;wf,14,1,0}
{ibba,bonds=1}
