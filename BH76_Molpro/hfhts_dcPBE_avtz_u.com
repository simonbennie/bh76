memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      1.13721778
   F          0.00000000      0.00000000      0.00000000
   H          0.00000000      0.00000000     -1.13721778
}
basis={ default,avtz }
{uhf,direct;wf,11,1,0}
{uks,PBE,direct;maxit,0;wf,11,1,0}
{ibba,bonds=1}
