memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000      0.00000000
}
basis={ default,avdz }
{df-ks,M06-HF,direct;wf,9,1,0}
{ibba,bonds=1}
