memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000     -0.09256457
   O          0.00000000      0.00000000      1.09674807
   H          0.00000000      0.00000000     -1.00418350
}
basis={ default,SV(P) }
{uks,M11-L,direct;wf,10,1,0}
{ibba,bonds=1}
