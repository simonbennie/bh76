memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.48102212
   H          0.00000000      0.00000000     -0.48102212
}
basis={ default,svp }
{uhf,direct;wf,9,1,0}
{uks,B3LYP5,direct;maxit,0;wf,9,1,0}
{ibba,bonds=1}
