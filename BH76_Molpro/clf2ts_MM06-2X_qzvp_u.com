memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL        -0.00292200     -2.08880019      0.00000000
   F         -0.00066800      0.05892918      0.00000000
   F          0.00359000      2.02987101      0.00000000
}
basis={ default,qzvp }
{uks,MM06-2X,direct;wf,35,1,0}
{ibba,bonds=1}
