memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.62463306     -0.70403964      0.00000000
   N         -0.31231653     -0.23709575      0.00000000
   N         -0.31231653      0.94113539      0.00000000
}
basis={ default,tzvp }
{hf;wf,15,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,15,1,0}
{ibba,bonds=1}
