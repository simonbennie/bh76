memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C          0.00000000      0.00000000     -1.11118191
   N          0.00000000      0.00000000      0.05815587
   H          0.00000000      0.00000000      1.05302603
}
basis={ default,tzvp }
{uks,M06,direct;disp;wf,14,1,0}
{ibba,bonds=1}
