memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.00000000      0.00000000      0.63722413
   H          0.00000000      0.00000000     -0.63722413
}
basis={ default,svp }
{hf,direct;wf,18,1,0}
{df-ks,M06-2X,direct;maxit,0;wf,18,1,0}
{ibba,bonds=1}
