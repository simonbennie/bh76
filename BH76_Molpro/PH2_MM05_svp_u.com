memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   P          0.00000000      0.00000000      0.65538955
   H         -1.02013202      0.00000000     -0.32769478
   H          1.02013202      0.00000000     -0.32769478
}
basis={ default,svp }
{uks,MM05,direct;wf,17,1,0}
{ibba,bonds=1}
