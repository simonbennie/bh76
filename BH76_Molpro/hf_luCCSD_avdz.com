memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000      0.45769122
   H          0.00000000      0.00000000     -0.45769122
}
basis={ default,avdz }
{hf;wf,10,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,10,1,0}
{ibba,bonds=1}
