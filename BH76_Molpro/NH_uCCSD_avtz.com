memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000      0.51836669
   H          0.00000000      0.00000000     -0.51836669
}
basis={ default,avtz }
{hf;wf,8,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,8,1,0}
{ibba,bonds=1}
