memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000     -0.25661586
   CL         0.00000000      0.00000000      1.28726458
   H          0.00000000      0.00000000     -1.03064872
}
basis={ default,avtz }
{hf;wf,19,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,19,1,0}
{ibba,bonds=1}
