memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.39048483
   H         -0.75670759      0.00000000     -0.19524242
   H          0.75670759      0.00000000     -0.19524242
}
basis={ default,tzvp }
{df-ks,MM05-2X,direct;wf,10,1,0}
{ibba,bonds=1}
