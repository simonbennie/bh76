memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000      0.28222663
   H          0.46901226      0.81235307     -0.09407554
   H          0.46901226     -0.81235307     -0.09407554
   H         -0.93802452      0.00000000     -0.09407554
}
basis={ default,SV(P) }
{uks,M06-2X,direct;disp;wf,10,1,0}
{ibba,bonds=1}
