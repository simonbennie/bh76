memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      0.37093843
   H          0.00000000      0.00000000     -0.37093843
}
basis={ default,qzvp }
{uks,VS99,direct;wf,2,1,0}
{ibba,bonds=1}
