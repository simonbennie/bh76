memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000     -0.09256457
   O          0.00000000      0.00000000      1.09674807
   H          0.00000000      0.00000000     -1.00418350
}
basis={ default,qzvp }
{hf;wf,10,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,10,1,0}
{ibba,bonds=1}
