memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.00000000
}
basis={ default,qzvp }
{df-ks,BH-LYP,direct;disp;wf,8,1,0}
{ibba,bonds=1}
