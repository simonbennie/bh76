memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.48444828
   H          0.00000000      0.00000000     -0.48444828
}
basis={ default,tzvp }
{uks,M06,direct;disp;wf,9,1,0}
{ibba,bonds=1}
