memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.00000000      0.00000000      0.63722413
   H          0.00000000      0.00000000     -0.63722413
}
basis={ default,tzvp }
{hf;wf,18,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,18,1,0}
{ibba,bonds=1}
