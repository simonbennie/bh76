memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      1.13721778
   F          0.00000000      0.00000000      0.00000000
   H          0.00000000      0.00000000     -1.13721778
}
basis={ default,avtz }
{hf;wf,11,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,11,1,0}
{ibba,bonds=1}
