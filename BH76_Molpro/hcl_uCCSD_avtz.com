memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.00000000      0.00000000      0.63722413
   H          0.00000000      0.00000000     -0.63722413
}
basis={ default,avtz }
{hf;wf,18,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,18,1,0}
{ibba,bonds=1}
