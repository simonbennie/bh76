memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   CL         0.00000000      0.00000000      0.00000000
}
basis={ default,avtz }
{uhf,direct;wf,17,1,0}
{uks,M06,direct;maxit,0;wf,17,1,0}
{ibba,bonds=1}
