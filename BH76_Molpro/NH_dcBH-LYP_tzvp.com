memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   N          0.00000000      0.00000000      0.51836669
   H          0.00000000      0.00000000     -0.51836669
}
basis={ default,tzvp }
{hf,direct;wf,8,1,0}
{df-ks,BH-LYP,direct;maxit,0;wf,8,1,0}
{ibba,bonds=1}
