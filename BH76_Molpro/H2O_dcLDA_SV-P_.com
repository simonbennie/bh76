memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.39048483
   H         -0.75670759      0.00000000     -0.19524242
   H          0.75670759      0.00000000     -0.19524242
}
basis={ default,SV(P) }
{hf,direct;wf,10,1,0}
{ks,LDA,direct;maxit,0;wf,10,1,0}
{ibba,bonds=1}
