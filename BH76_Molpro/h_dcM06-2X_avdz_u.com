memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      0.00000000
}
basis={ default,avdz }
{uhf,direct;wf,1,1,0}
{uks,M06-2X,direct;maxit,0;wf,1,1,0}
{ibba,bonds=1}
