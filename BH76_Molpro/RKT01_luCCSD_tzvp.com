memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000     -0.25661586
   CL         0.00000000      0.00000000      1.28726458
   H          0.00000000      0.00000000     -1.03064872
}
basis={ default,tzvp }
{hf;wf,19,1,0}
{df-luCCSD(T)-f12,ansatz=3*A(Fix,NoX),direct;wf,19,1,0}
{ibba,bonds=1}
