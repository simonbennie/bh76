memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   F          0.00000000      0.00000000     -1.99680889
   C          0.00000000      0.00000000      0.05274751
   H         -0.53104260      0.91979276     -0.07385286
   H         -0.53104260     -0.91979276     -0.07385286
   H          1.06208520      0.00000000     -0.07385286
   CL         0.00000000      0.00000000      2.16561997
}
basis={ default,SV(P) }
{uks,PBE,direct;wf,35,1,0}
{ibba,bonds=1}
