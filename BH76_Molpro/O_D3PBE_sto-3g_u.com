memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.00000000
}
basis={ default,sto-3g }
{uks,PBE,direct;disp;wf,8,1,0}
{ibba,bonds=1}
