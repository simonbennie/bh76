memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   C          0.00000000      0.00000000     -0.02902643
   N          0.00000000      0.00000000      1.12397830
   H          0.00000000      0.00000000     -1.09495187
}
basis={ default,avdz }
{uhf,direct;wf,14,1,0}
{uks,B-LYP,direct;maxit,0;wf,14,1,0}
{ibba,bonds=1}
