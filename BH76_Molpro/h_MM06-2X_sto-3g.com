memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   H          0.00000000      0.00000000      0.00000000
}
basis={ default,sto-3g }
{ks,MM06-2X,direct;wf,1,1,0}
{ibba,bonds=1}
