memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
   O          0.00000000      0.00000000      0.00000000
}
basis={ default,tzvp }
{uhf,direct;wf,8,1,0}
{uks,BH-LYP,direct;maxit,0;wf,8,1,0}
{ibba,bonds=1}
